libdatetime-timezone-tzfile-perl (0.011-2) unstable; urgency=medium

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Xavier Guimard ]
  * Email change: Xavier Guimard -> yadd@debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Mon, 13 Jun 2022 15:18:53 +0100

libdatetime-timezone-tzfile-perl (0.011-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Import upstream version 0.011
  * Update debian/upstream/metadata.
  * Update years of upstream copyright.
  * Declare compliance with Debian Policy 4.0.0.
  * Bump debhelper compatibility level to 9.

 -- gregor herrmann <gregoa@debian.org>  Wed, 02 Aug 2017 21:00:44 -0400

libdatetime-timezone-tzfile-perl (0.010-2) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * Add debian/upstream/metadata
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 3.9.6.
  * Add explicit build dependency on libmodule-build-perl.

 -- gregor herrmann <gregoa@debian.org>  Sun, 07 Jun 2015 17:51:23 +0200

libdatetime-timezone-tzfile-perl (0.010-1) unstable; urgency=low

  * Team upload.
  * New upstream release.
  * Bump version of libdatetime-timezone-systemv-perl (build) dependency.

 -- gregor herrmann <gregoa@debian.org>  Sat, 21 Sep 2013 17:43:42 +0200

libdatetime-timezone-tzfile-perl (0.008-1) unstable; urgency=low

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Xavier Guimard ]
  * Imported Upstream version 0.008
  * Update debian/copyright years
  * Add minimal version to libdatetime-timezone-systemv-perl dependency

 -- Xavier Guimard <x.guimard@free.fr>  Fri, 26 Jul 2013 04:44:36 +0200

libdatetime-timezone-tzfile-perl (0.007-1) unstable; urgency=low

  [ Xavier Guimard ]
  * Take over for the Debian Perl Group on maintainer's request
    (http://bugs.debian.org/677732#10)
  * Imported Upstream version 0.007
  * Update source format to 3.0 (quilt)
  * Bump Standards-Version to 3.9.4
  * Bump Standards-Version to 8
  * Add libparams-classify-perl in dependencies
  * Update debian/copyright (years and format)

  [ gregor herrmann ]
  * Don't install README.
  * Mention module name in long description.
  * Build depend on libtest-pod* for additional tests.

 -- Xavier Guimard <x.guimard@free.fr>  Sat, 22 Dec 2012 19:07:38 +0100

libdatetime-timezone-tzfile-perl (0.002-1) unstable; urgency=low

  * Initial Release (closes: Bug#575980).

 -- Ivan Kohler <ivan-debian@420.am>  Tue, 30 Mar 2010 21:06:36 -0700
